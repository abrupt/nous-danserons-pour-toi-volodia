# ~/ABRÜPT/PIER LAMPÁS/NOUS DANSERONS POUR TOI, VOLODIA !/*

La [page de ce livre](https://abrupt.cc/pier-lampas/nous-danserons-pour-toi-volodia/) sur le réseau.

## Sur le livre

à Budapest

sur les rives du Danube

faire danser les anges de l’histoire

## Sur l'auteur

Lutte, vagabonde, écrit dans le sud de l'Europe.

[Un site](https://pierlampas.com/) pour faire *livre d'image*.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
