﻿const danse = [
  ["massacrer les&nbsp;révoltes logiques",
   "sous&nbsp;l’approbation des&nbsp;anges"],
  ["aurons-nous droit",
   "à&nbsp;un&nbsp;dernier printemps",
   "avant l’apocalypse&nbsp;?"],
  ["d’une&nbsp;plainte étoilée",
   "à&nbsp;sa&nbsp;mort-manifeste"],
  ["bucolique insomnie",
   "et le&nbsp;refrain bavard",
   "des&nbsp;enfants de&nbsp;l’an iii"],
  ["à&nbsp;la&nbsp;poubelle de&nbsp;l’histoire&nbsp;!"],
  ["car il est loin le&nbsp;temps",
   "des&nbsp;internationales"],
  ["et nous sommes tous",
   "désormais les&nbsp;bâtards",
   "d’un&nbsp;ciel apolitique"],
  ["pareil à&nbsp;l’espérance",
   "d’une&nbsp;révolution spartakiste"],
  ["et nos&nbsp;barques comme les&nbsp;tiennes se sont éclatées",
   "contre des&nbsp;paysages utopiques"],
  ["une&nbsp;auréole industrielle",
   "sous&nbsp;l’éminence de&nbsp;notre&nbsp;modernité grise"],
  ["comme tous ces bâtiments neufs",
   "dans&nbsp;la&nbsp;banlieue de&nbsp;prague"],
  ["poèmes",
   "épiques",
   "d’architecture"],
  ["qui secoue l’avenir",
   "comme un&nbsp;sac de&nbsp;patates"],
  ["éclat mordoré de&nbsp;ta&nbsp;pratique"],
  ["marxiste",
   "moderniste",
   "épique"],
  ["propagande-insomnie&nbsp;!"],
  ["la&nbsp;fadeur",
   "du classicisme",
   "inquiet"],
  ["le&nbsp;rythme est le&nbsp;seul rapport concret à&nbsp;la&nbsp;totalité"],
  ["la&nbsp;force d’une&nbsp;langue n’est pas sa&nbsp;métrique"],
  ["le&nbsp;cri doit remplacer la&nbsp;chanson"],
  ["le&nbsp;verbe muscle l’imagination"],
  ["l’éclatement de&nbsp;la&nbsp;langue la&nbsp;casse de&nbsp;nos&nbsp;tourments"],
  ["se gavant de&nbsp;gloires&nbsp;&&nbsp;de&nbsp;délices",
   "prêt à&nbsp;tous les&nbsp;compromis"],
  ["dans&nbsp;une&nbsp;succursale de&nbsp;l’impérialisme moscovite"],
  ["la&nbsp;grande&nbsp;horloge de&nbsp;l’histoire s’est arrêtée à&nbsp;budapest"],
  ["car il n’y&nbsp;a plus de&nbsp;centre ni d’origine"],
  ["et nos&nbsp;rêves ont la&nbsp;texture d’un&nbsp;crépuscule hongrois"],
  ["révolution",
   "grande",
   "persuasion"],
  ["déracine nos&nbsp;étoiles rouges&nbsp;!"],
  ["arrache",
   "matraque"],
  ["allumer un&nbsp;grand feu de&nbsp;joie",
   "place de&nbsp;la&nbsp;concorde"],
  ["car notre&nbsp;raison est stratégique"],
  ["nous n’avons plus le&nbsp;temps",
   "pour les&nbsp;guerres de&nbsp;chapelle",
   "pour les&nbsp;assises de&nbsp;l’espérance mutique"],
  ["tenir la&nbsp;ligne&nbsp;!"],
  ["car le&nbsp;sang qui coule dans&nbsp;nos&nbsp;veines",
   "n’a pas la&nbsp;couleur fadasse des&nbsp;traîtres"],
  ["cette&nbsp;impossible dialectique"],
  ["tape tape tape",
   "contre le&nbsp;carrelage blanc",
   "de&nbsp;la&nbsp;petite-bourgeoisie"],
  ["que devons-nous faire",
   "des&nbsp;vieux airs",
   "d’avant 19... &nbsp;?"],
  ["le&nbsp;sismographe de&nbsp;la&nbsp;modernité s’est enraillé"],
  ["nous crachons sur les&nbsp;tapisseries de&nbsp;la&nbsp;république"],
  ["ce&nbsp;siècle n’en&nbsp;finit plus d’exhiber ses&nbsp;suicidés"],
  ["il n’y&nbsp;a pas de&nbsp;révolution sans&nbsp;patates sans&nbsp;marteaux sans&nbsp;sismographes"],
  ["il n’y&nbsp;a pas de&nbsp;révolution sans&nbsp;solitude&nbsp;sans&nbsp;étude&nbsp;sans&nbsp;critique de&nbsp;l’économie politique sans&nbsp;poésie"],
  ["toute révolution aura pour vocation de&nbsp;faire danser les&nbsp;anges de&nbsp;l’histoire"],
  ["seul avec un&nbsp;chant révolu"],
  ["seul avec un&nbsp;marteau trop lourd"],
  ["seul avec ta&nbsp;carcasse de&nbsp;géant"],
  ["seul avec ton&nbsp;crâne nu"],
  ["seul avec ta&nbsp;solitude&nbsp;immense"],
  ["seul avec staline"],
  ["seul avec les&nbsp;cris de&nbsp;désespoir et de&nbsp;joie"],
  ["rejoindre la&nbsp;cohue des&nbsp;analphabètes&nbsp;des&nbsp;schizophrènes des&nbsp;autistes des&nbsp;détraqués"],
  ["seul avec les&nbsp;anges de&nbsp;l’histoire"],
  ["délivré de&nbsp;rien&nbsp;du tout"],
  ["il y&nbsp;a dans&nbsp;l’art du sacrifice",
   "un&nbsp;résidu de&nbsp;morale orthodoxe",
   "qui pue le&nbsp;style de&nbsp;nos&nbsp;défaites"],
  ["la&nbsp;mort est un&nbsp;alibi"],
  ["au nom d’un&nbsp;vieux serment"],
  ["mobilisation massive",
   "grèves reconductibles",
   "en&nbsp;perspective"],
  ["l’empire n’a pas vaincu l’empire"],
  ["l’homme&nbsp;? la&nbsp;raison&nbsp;? l’histoire&nbsp;?"],
  ["ce&nbsp;qu’il nous reste&nbsp;?",
   "éclats de&nbsp;poudre et des&nbsp;éclairs"],
  ["un&nbsp;crâne rempli de&nbsp;poèmes à&nbsp;cracher aux&nbsp;étoiles"],
  ["et du feu que nous sommes"],
  ["la&nbsp;guerre est totale"],
  ["putréfaction des&nbsp;morales"],
  ["vieilles enclumes, nouveaux étendards"],
  ["paris est si belle sous&nbsp;la&nbsp;mitraille..."],
  ["nous réconcilier avec sa&nbsp;brume et son&nbsp;crachin"],
  ["que nos&nbsp;flammes aient l’audace de&nbsp;faire danser les&nbsp;petits matins gris de&nbsp;montparnasse"],
  ["automne magyar cigale patrie"],
  ["éventrer tous les&nbsp;artisanats nationaux, tous les&nbsp;corporatismes de&nbsp;la&nbsp;haine,",
   "tous ces carnavals de&nbsp;foutre et d’insanité homologués dans&nbsp;des&nbsp;ministères"],
  ["nos&nbsp;audaces, bien&nbsp;que sous-traitées,",
   "imparfaites, vacillantes, refuseront",
   "les&nbsp;sirènes assourdissantes du savoir"],
  ["nous n’aurons guère la&nbsp;nostalgie des&nbsp;conquêtes&nbsp;nationales et des&nbsp;grandeurs malades"],
  ["aucune&nbsp;appétence pour les&nbsp;écrivains nationaux"],
  ["nos&nbsp;traditions profanes court-circuiteront",
   "les&nbsp;récits bedonnants et leurs tours d’ivoire"],
  ["nous serons sans&nbsp;pitié pour ces malfrats des&nbsp;belles lettres"],
  ["leur univers de&nbsp;marbre et de&nbsp;ressentiment"],
  ["d’un&nbsp;pouvoir qui détruit méthodiquement toute possibilité d’errance, de&nbsp;fraternité, de&nbsp;bonté"],
  ["avis aux&nbsp;citoyens honorables, aux&nbsp;électeurs serviles, aux&nbsp;poireaux de&nbsp;l’audimat"],
  ["les&nbsp;révolutions ne se magouillent pas",
   "dans&nbsp;des&nbsp;ministères, dans&nbsp;des&nbsp;chambres amères,",
   "dans&nbsp;des&nbsp;meetings pour tribuns et autres vieux bigots patriotes"],
  ["nous croyons au style car nous n’avons que poudre et mirages à&nbsp;offrir"],
  ["dans&nbsp;l’espace-ouvert de&nbsp;nos&nbsp;devenirs"],
  ["nous traçons lueurs guidant nos&nbsp;corps infirmes,",
   "nos&nbsp;cerveaux malades, sur des&nbsp;sentiers épiques et doux"],
  ["et qu’importe la&nbsp;qualité de&nbsp;nos&nbsp;lames"],
  ["nous doutons de&nbsp;nos&nbsp;propres larmes"],
  ["le&nbsp;nerf ébahi plonge l’accord dans&nbsp;le&nbsp;fiel de&nbsp;l’oubli"],
  ["nos&nbsp;justes résistances n’ont plus de&nbsp;sol ni de&nbsp;maquis"],
  ["nous nous sommes faits amants des&nbsp;braises"],
  ["nous accouchons d’astres matériels"],
  ["nos&nbsp;nuits épiques, nos&nbsp;jours béants,",
   "nos&nbsp;fêlures autant que nos&nbsp;trésors,",
   "l’aube acide&nbsp;et l’hiver hystérique"],
  ["qu’importe&nbsp;!",
   "c’est ici-bas que nous creusons&nbsp;:",
   "terre, bitume, ciels&nbsp;!"],
  ["entends ce&nbsp;feu qui chante, cette&nbsp;rose",
   "sans&nbsp;concession échouée sur des&nbsp;parpaings glacés"],
  ["entends la&nbsp;différence, la&nbsp;multitude, l’écart"],
  ["errance &&nbsp;collusion"],
  ["une&nbsp;bouche effarante, maniant l’outrance et la&nbsp;dentelle"],
  ["le&nbsp;verbe et sa&nbsp;nécrose"],
  ["est lyrisme en&nbsp;sa&nbsp;brisure",
   "est idéal en&nbsp;son&nbsp;écorchure",
   "est occident en&nbsp;son&nbsp;embrasement"],
  ["qu’il s’agisse d’affronter des&nbsp;empires"],
  ["conjurer le&nbsp;poison des&nbsp;nations"],
  ["désarmer l’effroi, de&nbsp;désarmer l’état"],
  ["abolir nos&nbsp;murs de&nbsp;glace et de&nbsp;résignation"],
  ["une&nbsp;dialectique intempestive qui liera ciel&nbsp;&&nbsp;terre"],
  ["le&nbsp;poème au combat"],
  ["le&nbsp;nouveau fascisme est celui de&nbsp;la&nbsp;mort des&nbsp;perspectives utopiques"],
  ["la&nbsp;relégation infinie des&nbsp;possibles"],
  ["la&nbsp;structuration universelle-marchande&nbsp;de&nbsp;l’ensemble des&nbsp;rapports sociaux"],
  ["règne de&nbsp;la&nbsp;rationalité exclusivement économique"],
  ["notre&nbsp;unique alternative, c’est l’utopie concrète"],
  ["la&nbsp;destruction du kapital, de&nbsp;ses&nbsp;enclaves gestionnaires, de&nbsp;tous ses&nbsp;étendards"],
  ["frontières balises cartographies du pire"],
  ["et nous, brume, crachin, aube incertaine, privilège de&nbsp;braise et de&nbsp;bas-fonds"],
  ["nous sommes la&nbsp;bouche cassée des&nbsp;tempêtes"],
  ["nous habitons la&nbsp;brèche"],
  ["notre&nbsp;noblesse animale sous&nbsp;de&nbsp;méchantes rocades"],
  ["quand nos&nbsp;sœurs recluses s’armeront de&nbsp;glaives et de&nbsp;marteaux"],
  ["nous nous jetterons",
   "à&nbsp;l’assaut des&nbsp;nuits brunes",
   "et nous brûlerons"],
  ["à&nbsp;l’assaut des&nbsp;mondes",
   "dans&nbsp;le&nbsp;feu béni des&nbsp;révolutions"],
  ["et nous aurons à&nbsp;cœur",
   "de&nbsp;réveiller des&nbsp;morts"],
  ["souffler sur les&nbsp;braises"],
  ["de&nbsp;verser des&nbsp;torrents de&nbsp;brume légère"],
  ["à&nbsp;la&nbsp;lumière enfin",
   "que déverse la&nbsp;plainte éperdue",
   "de&nbsp;ce&nbsp;volcan éteint"],
];
