const danse = [
  ["massacrer les&nbsp;révoltes logiques",
   "sous&nbsp;l’approbation des&nbsp;anges"],
  ["aurons-nous droit",
   "à&nbsp;un&nbsp;dernier printemps",
   "avant l’apocalypse&nbsp;?"],
  ["d’une&nbsp;plainte étoilée",
   "à&nbsp;sa&nbsp;mort-manifeste"],
  ["bucolique insomnie",
   "et le&nbsp;refrain bavard",
   "des&nbsp;enfants de&nbsp;l’an iii"],
  ["à&nbsp;la&nbsp;poubelle de&nbsp;l’histoire&nbsp;!"],
  ["car il est loin le&nbsp;temps",
   "des&nbsp;internationales"],
  ["et nous sommes tous",
   "désormais les&nbsp;bâtards",
   "d’un&nbsp;ciel apolitique"],
  ["pareil à&nbsp;l’espérance",
   "d’une&nbsp;révolution spartakiste"],
  ["et nos&nbsp;barques comme les&nbsp;tiennes se sont éclatées",
   "contre des&nbsp;paysages utopiques"],
  ["une&nbsp;auréole industrielle",
   "sous&nbsp;l’éminence de&nbsp;notre&nbsp;modernité grise"],
  ["comme tous ces bâtiments neufs",
   "dans&nbsp;la&nbsp;banlieue de&nbsp;prague"],
  ["poèmes",
   "épiques",
   "d’architecture"],
  ["qui secoue l’avenir",
   "comme un&nbsp;sac de&nbsp;patates"],
  ["éclat mordoré de&nbsp;ta&nbsp;pratique"],
  ["marxiste",
   "moderniste",
   "épique"],
  ["propagande-insomnie&nbsp;!"],
  ["la&nbsp;fadeur",
   "du classicisme",
   "inquiet"],
  ["le&nbsp;rythme est le&nbsp;seul rapport concret à&nbsp;la&nbsp;totalité"],
  ["la&nbsp;force d’une&nbsp;langue n’est pas sa&nbsp;métrique"],
  ["le&nbsp;cri doit remplacer la&nbsp;chanson"],
  ["le&nbsp;verbe muscle l’imagination"],
  ["l’éclatement de&nbsp;la&nbsp;langue la&nbsp;casse de&nbsp;nos&nbsp;tourments"],
  ["se gavant de&nbsp;gloires&nbsp;&&nbsp;de&nbsp;délices",
   "prêt à&nbsp;tous les&nbsp;compromis"],
  ["dans&nbsp;une&nbsp;succursale de&nbsp;l’impérialisme moscovite"],
  ["la&nbsp;grande&nbsp;horloge de&nbsp;l’histoire s’est arrêtée à&nbsp;budapest"],
  ["car il n’y&nbsp;a plus de&nbsp;centre ni d’origine"],
  ["et nos&nbsp;rêves ont la&nbsp;texture d’un&nbsp;crépuscule hongrois"],
  ["révolution",
   "grande",
   "persuasion"],
  ["déracine nos&nbsp;étoiles rouges&nbsp;!"],
  ["arrache",
   "matraque"],
  ["allumer un&nbsp;grand feu de&nbsp;joie",
   "place de&nbsp;la&nbsp;concorde"],
  ["car notre&nbsp;raison est stratégique"],
  ["nous n’avons plus le&nbsp;temps",
   "pour les&nbsp;guerres de&nbsp;chapelle",
   "pour les&nbsp;assises de&nbsp;l’espérance mutique"],
  ["tenir la&nbsp;ligne&nbsp;!"],
  ["car le&nbsp;sang qui coule dans&nbsp;nos&nbsp;veines",
   "n’a pas la&nbsp;couleur fadasse des&nbsp;traîtres"],
  ["cette&nbsp;impossible dialectique"],
  ["tape tape tape",
   "contre le&nbsp;carrelage blanc",
   "de&nbsp;la&nbsp;petite-bourgeoisie"],
  ["que devons-nous faire",
   "des&nbsp;vieux airs",
   "d’avant 19... &nbsp;?"],
  ["le&nbsp;sismographe de&nbsp;la&nbsp;modernité s’est enraillé"],
  ["nous crachons sur les&nbsp;tapisseries de&nbsp;la&nbsp;république"],
  ["ce&nbsp;siècle n’en&nbsp;finit plus d’exhiber ses&nbsp;suicidés"],
  ["il n’y&nbsp;a pas de&nbsp;révolution sans&nbsp;patates sans&nbsp;marteaux sans&nbsp;sismographes"],
  ["il n’y&nbsp;a pas de&nbsp;révolution sans&nbsp;solitude&nbsp;sans&nbsp;étude&nbsp;sans&nbsp;critique de&nbsp;l’économie politique sans&nbsp;poésie"],
  ["toute révolution aura pour vocation de&nbsp;faire danser les&nbsp;anges de&nbsp;l’histoire"],
  ["seul avec un&nbsp;chant révolu"],
  ["seul avec un&nbsp;marteau trop lourd"],
  ["seul avec ta&nbsp;carcasse de&nbsp;géant"],
  ["seul avec ton&nbsp;crâne nu"],
  ["seul avec ta&nbsp;solitude&nbsp;immense"],
  ["seul avec staline"],
  ["seul avec les&nbsp;cris de&nbsp;désespoir et de&nbsp;joie"],
  ["rejoindre la&nbsp;cohue des&nbsp;analphabètes&nbsp;des&nbsp;schizophrènes des&nbsp;autistes des&nbsp;détraqués"],
  ["seul avec les&nbsp;anges de&nbsp;l’histoire"],
  ["délivré de&nbsp;rien&nbsp;du tout"],
  ["il y&nbsp;a dans&nbsp;l’art du sacrifice",
   "un&nbsp;résidu de&nbsp;morale orthodoxe",
   "qui pue le&nbsp;style de&nbsp;nos&nbsp;défaites"],
  ["la&nbsp;mort est un&nbsp;alibi"],
  ["au nom d’un&nbsp;vieux serment"],
  ["mobilisation massive",
   "grèves reconductibles",
   "en&nbsp;perspective"],
  ["l’empire n’a pas vaincu l’empire"],
  ["l’homme&nbsp;? la&nbsp;raison&nbsp;? l’histoire&nbsp;?"],
  ["ce&nbsp;qu’il nous reste&nbsp;?",
   "éclats de&nbsp;poudre et des&nbsp;éclairs"],
  ["un&nbsp;crâne rempli de&nbsp;poèmes à&nbsp;cracher aux&nbsp;étoiles"],
  ["et du feu que nous sommes"],
  ["la&nbsp;guerre est totale"],
  ["putréfaction des&nbsp;morales"],
  ["vieilles enclumes, nouveaux étendards"],
  ["paris est si belle sous&nbsp;la&nbsp;mitraille..."],
  ["nous réconcilier avec sa&nbsp;brume et son&nbsp;crachin"],
  ["que nos&nbsp;flammes aient l’audace de&nbsp;faire danser les&nbsp;petits matins gris de&nbsp;montparnasse"],
  ["automne magyar cigale patrie"],
  ["éventrer tous les&nbsp;artisanats nationaux, tous les&nbsp;corporatismes de&nbsp;la&nbsp;haine,",
   "tous ces carnavals de&nbsp;foutre et d’insanité homologués dans&nbsp;des&nbsp;ministères"],
  ["nos&nbsp;audaces, bien&nbsp;que sous-traitées,",
   "imparfaites, vacillantes, refuseront",
   "les&nbsp;sirènes assourdissantes du savoir"],
  ["nous n’aurons guère la&nbsp;nostalgie des&nbsp;conquêtes&nbsp;nationales et des&nbsp;grandeurs malades"],
  ["aucune&nbsp;appétence pour les&nbsp;écrivains nationaux"],
  ["nos&nbsp;traditions profanes court-circuiteront",
   "les&nbsp;récits bedonnants et leurs tours d’ivoire"],
  ["nous serons sans&nbsp;pitié pour ces malfrats des&nbsp;belles lettres"],
  ["leur univers de&nbsp;marbre et de&nbsp;ressentiment"],
  ["d’un&nbsp;pouvoir qui détruit méthodiquement toute possibilité d’errance, de&nbsp;fraternité, de&nbsp;bonté"],
  ["avis aux&nbsp;citoyens honorables, aux&nbsp;électeurs serviles, aux&nbsp;poireaux de&nbsp;l’audimat"],
  ["les&nbsp;révolutions ne se magouillent pas",
   "dans&nbsp;des&nbsp;ministères, dans&nbsp;des&nbsp;chambres amères,",
   "dans&nbsp;des&nbsp;meetings pour tribuns et autres vieux bigots patriotes"],
  ["nous croyons au style car nous n’avons que poudre et mirages à&nbsp;offrir"],
  ["dans&nbsp;l’espace-ouvert de&nbsp;nos&nbsp;devenirs"],
  ["nous traçons lueurs guidant nos&nbsp;corps infirmes,",
   "nos&nbsp;cerveaux malades, sur des&nbsp;sentiers épiques et doux"],
  ["et qu’importe la&nbsp;qualité de&nbsp;nos&nbsp;lames"],
  ["nous doutons de&nbsp;nos&nbsp;propres larmes"],
  ["le&nbsp;nerf ébahi plonge l’accord dans&nbsp;le&nbsp;fiel de&nbsp;l’oubli"],
  ["nos&nbsp;justes résistances n’ont plus de&nbsp;sol ni de&nbsp;maquis"],
  ["nous nous sommes faits amants des&nbsp;braises"],
  ["nous accouchons d’astres matériels"],
  ["nos&nbsp;nuits épiques, nos&nbsp;jours béants,",
   "nos&nbsp;fêlures autant que nos&nbsp;trésors,",
   "l’aube acide&nbsp;et l’hiver hystérique"],
  ["qu’importe&nbsp;!",
   "c’est ici-bas que nous creusons&nbsp;:",
   "terre, bitume, ciels&nbsp;!"],
  ["entends ce&nbsp;feu qui chante, cette&nbsp;rose",
   "sans&nbsp;concession échouée sur des&nbsp;parpaings glacés"],
  ["entends la&nbsp;différence, la&nbsp;multitude, l’écart"],
  ["errance &&nbsp;collusion"],
  ["une&nbsp;bouche effarante, maniant l’outrance et la&nbsp;dentelle"],
  ["le&nbsp;verbe et sa&nbsp;nécrose"],
  ["est lyrisme en&nbsp;sa&nbsp;brisure",
   "est idéal en&nbsp;son&nbsp;écorchure",
   "est occident en&nbsp;son&nbsp;embrasement"],
  ["qu’il s’agisse d’affronter des&nbsp;empires"],
  ["conjurer le&nbsp;poison des&nbsp;nations"],
  ["désarmer l’effroi, de&nbsp;désarmer l’état"],
  ["abolir nos&nbsp;murs de&nbsp;glace et de&nbsp;résignation"],
  ["une&nbsp;dialectique intempestive qui liera ciel&nbsp;&&nbsp;terre"],
  ["le&nbsp;poème au combat"],
  ["le&nbsp;nouveau fascisme est celui de&nbsp;la&nbsp;mort des&nbsp;perspectives utopiques"],
  ["la&nbsp;relégation infinie des&nbsp;possibles"],
  ["la&nbsp;structuration universelle-marchande&nbsp;de&nbsp;l’ensemble des&nbsp;rapports sociaux"],
  ["règne de&nbsp;la&nbsp;rationalité exclusivement économique"],
  ["notre&nbsp;unique alternative, c’est l’utopie concrète"],
  ["la&nbsp;destruction du kapital, de&nbsp;ses&nbsp;enclaves gestionnaires, de&nbsp;tous ses&nbsp;étendards"],
  ["frontières balises cartographies du pire"],
  ["et nous, brume, crachin, aube incertaine, privilège de&nbsp;braise et de&nbsp;bas-fonds"],
  ["nous sommes la&nbsp;bouche cassée des&nbsp;tempêtes"],
  ["nous habitons la&nbsp;brèche"],
  ["notre&nbsp;noblesse animale sous&nbsp;de&nbsp;méchantes rocades"],
  ["quand nos&nbsp;sœurs recluses s’armeront de&nbsp;glaives et de&nbsp;marteaux"],
  ["nous nous jetterons",
   "à&nbsp;l’assaut des&nbsp;nuits brunes",
   "et nous brûlerons"],
  ["à&nbsp;l’assaut des&nbsp;mondes",
   "dans&nbsp;le&nbsp;feu béni des&nbsp;révolutions"],
  ["et nous aurons à&nbsp;cœur",
   "de&nbsp;réveiller des&nbsp;morts"],
  ["souffler sur les&nbsp;braises"],
  ["de&nbsp;verser des&nbsp;torrents de&nbsp;brume légère"],
  ["à&nbsp;la&nbsp;lumière enfin",
   "que déverse la&nbsp;plainte éperdue",
   "de&nbsp;ce&nbsp;volcan éteint"],
];

/*! dom-to-image 10-06-2017 */
!function(a){"use strict";function b(a,b){function c(a){return b.bgcolor&&(a.style.backgroundColor=b.bgcolor),b.width&&(a.style.width=b.width+"px"),b.height&&(a.style.height=b.height+"px"),b.style&&Object.keys(b.style).forEach(function(c){a.style[c]=b.style[c]}),a}return b=b||{},g(b),Promise.resolve(a).then(function(a){return i(a,b.filter,!0)}).then(j).then(k).then(c).then(function(c){return l(c,b.width||q.width(a),b.height||q.height(a))})}function c(a,b){return h(a,b||{}).then(function(b){return b.getContext("2d").getImageData(0,0,q.width(a),q.height(a)).data})}function d(a,b){return h(a,b||{}).then(function(a){return a.toDataURL()})}function e(a,b){return b=b||{},h(a,b).then(function(a){return a.toDataURL("image/jpeg",b.quality||1)})}function f(a,b){return h(a,b||{}).then(q.canvasToBlob)}function g(a){"undefined"==typeof a.imagePlaceholder?v.impl.options.imagePlaceholder=u.imagePlaceholder:v.impl.options.imagePlaceholder=a.imagePlaceholder,"undefined"==typeof a.cacheBust?v.impl.options.cacheBust=u.cacheBust:v.impl.options.cacheBust=a.cacheBust}function h(a,c){function d(a){var b=document.createElement("canvas");if(b.width=c.width||q.width(a),b.height=c.height||q.height(a),c.bgcolor){var d=b.getContext("2d");d.fillStyle=c.bgcolor,d.fillRect(0,0,b.width,b.height)}return b}return b(a,c).then(q.makeImage).then(q.delay(100)).then(function(b){var c=d(a);return c.getContext("2d").drawImage(b,0,0),c})}function i(a,b,c){function d(a){return a instanceof HTMLCanvasElement?q.makeImage(a.toDataURL()):a.cloneNode(!1)}function e(a,b,c){function d(a,b,c){var d=Promise.resolve();return b.forEach(function(b){d=d.then(function(){return i(b,c)}).then(function(b){b&&a.appendChild(b)})}),d}var e=a.childNodes;return 0===e.length?Promise.resolve(b):d(b,q.asArray(e),c).then(function(){return b})}function f(a,b){function c(){function c(a,b){function c(a,b){q.asArray(a).forEach(function(c){b.setProperty(c,a.getPropertyValue(c),a.getPropertyPriority(c))})}a.cssText?b.cssText=a.cssText:c(a,b)}c(window.getComputedStyle(a),b.style)}function d(){function c(c){function d(a,b,c){function d(a){var b=a.getPropertyValue("content");return a.cssText+" content: "+b+";"}function e(a){function b(b){return b+": "+a.getPropertyValue(b)+(a.getPropertyPriority(b)?" !important":"")}return q.asArray(a).map(b).join("; ")+";"}var f="."+a+":"+b,g=c.cssText?d(c):e(c);return document.createTextNode(f+"{"+g+"}")}var e=window.getComputedStyle(a,c),f=e.getPropertyValue("content");if(""!==f&&"none"!==f){var g=q.uid();b.className=b.className+" "+g;var h=document.createElement("style");h.appendChild(d(g,c,e)),b.appendChild(h)}}[":before",":after"].forEach(function(a){c(a)})}function e(){a instanceof HTMLTextAreaElement&&(b.innerHTML=a.value),a instanceof HTMLInputElement&&b.setAttribute("value",a.value)}function f(){b instanceof SVGElement&&(b.setAttribute("xmlns","http://www.w3.org/2000/svg"),b instanceof SVGRectElement&&["width","height"].forEach(function(a){var c=b.getAttribute(a);c&&b.style.setProperty(a,c)}))}return b instanceof Element?Promise.resolve().then(c).then(d).then(e).then(f).then(function(){return b}):b}return c||!b||b(a)?Promise.resolve(a).then(d).then(function(c){return e(a,c,b)}).then(function(b){return f(a,b)}):Promise.resolve()}function j(a){return s.resolveAll().then(function(b){var c=document.createElement("style");return a.appendChild(c),c.appendChild(document.createTextNode(b)),a})}function k(a){return t.inlineAll(a).then(function(){return a})}function l(a,b,c){return Promise.resolve(a).then(function(a){return a.setAttribute("xmlns","http://www.w3.org/1999/xhtml"),(new XMLSerializer).serializeToString(a)}).then(q.escapeXhtml).then(function(a){return'<foreignObject x="0" y="0" width="100%" height="100%">'+a+"</foreignObject>"}).then(function(a){return'<svg xmlns="http://www.w3.org/2000/svg" width="'+b+'" height="'+c+'">'+a+"</svg>"}).then(function(a){return"data:image/svg+xml;charset=utf-8,"+a})}function m(){function a(){var a="application/font-woff",b="image/jpeg";return{woff:a,woff2:a,ttf:"application/font-truetype",eot:"application/vnd.ms-fontobject",png:"image/png",jpg:b,jpeg:b,gif:"image/gif",tiff:"image/tiff",svg:"image/svg+xml"}}function b(a){var b=/\.([^\.\/]*?)$/g.exec(a);return b?b[1]:""}function c(c){var d=b(c).toLowerCase();return a()[d]||""}function d(a){return a.search(/^(data:)/)!==-1}function e(a){return new Promise(function(b){for(var c=window.atob(a.toDataURL().split(",")[1]),d=c.length,e=new Uint8Array(d),f=0;f<d;f++)e[f]=c.charCodeAt(f);b(new Blob([e],{type:"image/png"}))})}function f(a){return a.toBlob?new Promise(function(b){a.toBlob(b)}):e(a)}function g(a,b){var c=document.implementation.createHTMLDocument(),d=c.createElement("base");c.head.appendChild(d);var e=c.createElement("a");return c.body.appendChild(e),d.href=b,e.href=a,e.href}function h(){var a=0;return function(){function b(){return("0000"+(Math.random()*Math.pow(36,4)<<0).toString(36)).slice(-4)}return"u"+b()+a++}}function i(a){return new Promise(function(b,c){var d=new Image;d.onload=function(){b(d)},d.onerror=c,d.src=a})}function j(a){var b=3e4;return v.impl.options.cacheBust&&(a+=(/\?/.test(a)?"&":"?")+(new Date).getTime()),new Promise(function(c){function d(){if(4===g.readyState){if(200!==g.status)return void(h?c(h):f("cannot fetch resource: "+a+", status: "+g.status));var b=new FileReader;b.onloadend=function(){var a=b.result.split(/,/)[1];c(a)},b.readAsDataURL(g.response)}}function e(){h?c(h):f("timeout of "+b+"ms occured while fetching resource: "+a)}function f(a){console.error(a),c("")}var g=new XMLHttpRequest;g.onreadystatechange=d,g.ontimeout=e,g.responseType="blob",g.timeout=b,g.open("GET",a,!0),g.send();var h;if(v.impl.options.imagePlaceholder){var i=v.impl.options.imagePlaceholder.split(/,/);i&&i[1]&&(h=i[1])}})}function k(a,b){return"data:"+b+";base64,"+a}function l(a){return a.replace(/([.*+?^${}()|\[\]\/\\])/g,"\\$1")}function m(a){return function(b){return new Promise(function(c){setTimeout(function(){c(b)},a)})}}function n(a){for(var b=[],c=a.length,d=0;d<c;d++)b.push(a[d]);return b}function o(a){return a.replace(/#/g,"%23").replace(/\n/g,"%0A")}function p(a){var b=r(a,"border-left-width"),c=r(a,"border-right-width");return a.scrollWidth+b+c}function q(a){var b=r(a,"border-top-width"),c=r(a,"border-bottom-width");return a.scrollHeight+b+c}function r(a,b){var c=window.getComputedStyle(a).getPropertyValue(b);return parseFloat(c.replace("px",""))}return{escape:l,parseExtension:b,mimeType:c,dataAsUrl:k,isDataUrl:d,canvasToBlob:f,resolveUrl:g,getAndEncode:j,uid:h(),delay:m,asArray:n,escapeXhtml:o,makeImage:i,width:p,height:q}}function n(){function a(a){return a.search(e)!==-1}function b(a){for(var b,c=[];null!==(b=e.exec(a));)c.push(b[1]);return c.filter(function(a){return!q.isDataUrl(a)})}function c(a,b,c,d){function e(a){return new RegExp("(url\\(['\"]?)("+q.escape(a)+")(['\"]?\\))","g")}return Promise.resolve(b).then(function(a){return c?q.resolveUrl(a,c):a}).then(d||q.getAndEncode).then(function(a){return q.dataAsUrl(a,q.mimeType(b))}).then(function(c){return a.replace(e(b),"$1"+c+"$3")})}function d(d,e,f){function g(){return!a(d)}return g()?Promise.resolve(d):Promise.resolve(d).then(b).then(function(a){var b=Promise.resolve(d);return a.forEach(function(a){b=b.then(function(b){return c(b,a,e,f)})}),b})}var e=/url\(['"]?([^'"]+?)['"]?\)/g;return{inlineAll:d,shouldProcess:a,impl:{readUrls:b,inline:c}}}function o(){function a(){return b(document).then(function(a){return Promise.all(a.map(function(a){return a.resolve()}))}).then(function(a){return a.join("\n")})}function b(){function a(a){return a.filter(function(a){return a.type===CSSRule.FONT_FACE_RULE}).filter(function(a){return r.shouldProcess(a.style.getPropertyValue("src"))})}function b(a){var b=[];return a.forEach(function(a){try{q.asArray(a.cssRules||[]).forEach(b.push.bind(b))}catch(c){console.log("Error while reading CSS rules from "+a.href,c.toString())}}),b}function c(a){return{resolve:function(){var b=(a.parentStyleSheet||{}).href;return r.inlineAll(a.cssText,b)},src:function(){return a.style.getPropertyValue("src")}}}return Promise.resolve(q.asArray(document.styleSheets)).then(b).then(a).then(function(a){return a.map(c)})}return{resolveAll:a,impl:{readAll:b}}}function p(){function a(a){function b(b){return q.isDataUrl(a.src)?Promise.resolve():Promise.resolve(a.src).then(b||q.getAndEncode).then(function(b){return q.dataAsUrl(b,q.mimeType(a.src))}).then(function(b){return new Promise(function(c,d){a.onload=c,a.onerror=d,a.src=b})})}return{inline:b}}function b(c){function d(a){var b=a.style.getPropertyValue("background");return b?r.inlineAll(b).then(function(b){a.style.setProperty("background",b,a.style.getPropertyPriority("background"))}).then(function(){return a}):Promise.resolve(a)}return c instanceof Element?d(c).then(function(){return c instanceof HTMLImageElement?a(c).inline():Promise.all(q.asArray(c.childNodes).map(function(a){return b(a)}))}):Promise.resolve(c)}return{inlineAll:b,impl:{newImage:a}}}var q=m(),r=n(),s=o(),t=p(),u={imagePlaceholder:void 0,cacheBust:!1},v={toSvg:b,toPng:d,toJpeg:e,toBlob:f,toPixelData:c,impl:{fontFaces:s,images:t,util:q,inliner:r,options:{}}};"undefined"!=typeof module?module.exports=v:a.domtoimage=v}(this);
// Scripts
const fabrique = document.querySelector('.button--shuffle');
const afficheTexte = document.querySelector('.contenu__texte');
const afficheGraphique = document.querySelector('.contenu__graphique');
const infoBtn = document.querySelector('.button--info');
const paysageBtn = document.querySelector('.button--paysage');
const info = document.querySelector('.informations');
const background = document.querySelector('.background');

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Affiche() {
  shuffleArray(danse);
  let texteFinal = "";

  for (let i = 0; i < danse[0].length; i++) {
    texteFinal += '<p>';
    texteFinal += danse[0][i];
    texteFinal += '</p>';
  }

  afficheTexte.innerHTML = texteFinal;

  paint();

}

fabrique.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  Affiche();
});

infoBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('informations--show');
});

function randomNb(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function paint() {

  const plusOrMinus = Math.random() < 0.5 ? -1 : 1;

  const existingEl = document.querySelectorAll('.contenu__graphique *, .background *');
  existingEl.forEach(function(el){
    el.remove();
  });

  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttributeNS(null, 'viewBox', '0 0 100 100');

  // Background
  const trapezoid = document.createElementNS('http://www.w3.org/2000/svg', 'path');

  const debutX = randomNb(20,80);
  const suiteX = randomNb(20,80);
  const finX = randomNb(0,100);
  const finY = randomNb(0,100);

  trapezoid.setAttributeNS(null, 'd', "M"+debutX+",0 L"+suiteX+",100 H200 V-100 L"+debutX+",0z");
  trapezoid.classList.add('trapeze');
  trapezoid.setAttributeNS(null, 'stroke', 'white');
  trapezoid.setAttributeNS(null, 'fill', 'white');
  if (plusOrMinus === 1) {
    trapezoid.setAttributeNS(null, 'transform', 'scale(-1, 1) translate(-100, 0)');
  }
  svg.appendChild(trapezoid);

  //small circles & triangles
  for (var i = 0; i < randomNb(3,7); i++) {
    const plusOrMinusBis = Math.random() < 0.5 ? -1 : 1;

    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circle.classList.add('circle');
    circle.setAttributeNS(null, 'cx', randomNb(0,100));
    circle.setAttributeNS(null, 'cy', randomNb(0,100));
    circle.setAttributeNS(null, 'r', randomNb(5,30));
    if (plusOrMinusBis === 1) {
      circle.setAttributeNS(null, 'fill', 'black');
    } else {
      circle.setAttributeNS(null, 'fill', 'white');
    }
    svg.appendChild(circle);
  }
  
  for (var i = 0; i < randomNb(3,7); i++) {
    const plusOrMinusBis = Math.random() < 0.5 ? -1 : 1;

    const triangle = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    triangle.classList.add('triangle');
    triangle.setAttributeNS(null, 'points', ''+ randomNb(0,100)+','+randomNb(0,100)+' '+randomNb(0,100)+','+randomNb(0,100)+' '+randomNb(0,100)+','+randomNb(0,100));
    if (plusOrMinusBis === 1) {
      triangle.setAttributeNS(null, 'fill', 'black');
    } else {
      triangle.setAttributeNS(null, 'fill', 'white');
    }
    svg.appendChild(triangle);
  }

  const svgBg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svgBg.setAttributeNS(null, 'viewBox', '0 0 100 100');

  //triangles background
  for (var i = 0; i < randomNb(5,10); i++) {
    // const randomColor = ['rgb(' + randomNb(0,255) + ',' + randomNb(0,255) + ',' + randomNb(0,255) + ')', "#ffdd00", "#ff0000", "#00dd11", "#0000ff"]
    // shuffleArray(randomColor);
    const triangleBg = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
    triangleBg.classList.add('triangle');
    triangleBg.setAttributeNS(null, 'points', ''+ randomNb(-10,110)+','+randomNb(-10,110)+' '+randomNb(-10,110)+','+randomNb(-10,110)+' '+randomNb(-10,110)+','+randomNb(-10,110));
    // shuffleArray(randomColor);
    triangleBg.setAttributeNS(null, 'fill', '#974aff');
    // triangleBg.setAttributeNS(null, 'stroke', randomColor[0]);
    // shuffleArray(randomColor);
    // triangleBg.setAttributeNS(null, 'fill', randomColor[0]);
    svgBg.appendChild(triangleBg);

    const circleBg = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circleBg.classList.add('circle');
    circleBg.setAttributeNS(null, 'cx', randomNb(-10,110));
    circleBg.setAttributeNS(null, 'cy', randomNb(-1,110));
    circleBg.setAttributeNS(null, 'r', randomNb(3,30));
    circleBg.setAttributeNS(null, 'fill', '#974aff');
    // shuffleArray(randomColor);
    // circleBg.setAttributeNS(null, 'stroke', randomColor[0]);
    // shuffleArray(randomColor);
    // circleBg.setAttributeNS(null, 'fill', randomColor[0]);
    svgBg.appendChild(circleBg);
  }

  background.appendChild(svgBg);

  afficheGraphique.appendChild(svg);

}

let downloadImage = document.querySelector('.button--img');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  document.documentElement.classList.add("hide-scrollbar");

var node = document.querySelector('.canvas');
const scale = 4200 / node.offsetWidth;

  domtoimage.toPng(node, {
        height: node.offsetHeight * scale,
    width: node.offsetWidth * scale,
    style: {
    transform: "scale(" + scale + ")",
    transformOrigin: "top left",
    width: node.offsetWidth + "px",
    height: node.offsetHeight + "px"
    }
  })
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'nous-danserons-pour-toi-volodia.png';
        link.href = dataUrl;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    })
    .catch(function (error) {
        console.error('oops, something went wrong!', error);
    });

  document.documentElement.classList.remove("hide-scrollbar");
});

Affiche();
